<section epub:type="chapter" role="doc-chapter">

# Enfoques WYSIWYG y WYSIWYM

En general, puede decirse que desde mediados de los ochenta surgieron,
además de los lenguajes de marcado, dos enfoques relacionados al tratamiento del 
texto digital: WYSIWYG y WYSIWYM. Ambos tienen una postura distinta en la 
relación entre la estructura y el diseño de una publicación.

![Mismo texto en diferentes capas; izquierda: capa visible y de diseño; derecha: capa estructural.](../img/s06_img01.jpg)

## Recursos

1. «[Historia de la edición digital](http://marianaeguaras.com/historia-de-la-edicion-digital/)».

----

## WYSIWYG

WYSIWYG es el acrónimo de *What You See Is What You Get*, con el cual 
se hace patente que su aproximación estriba en dotarle al usuario de la mayor
flexibilidad posible en el formato de un texto, principalmente mediante
la introducción de estilos directos. Esta versatilidad no solo permite
al usuario visualizar inmediatamente sus modificaciones, sino que
también respeta el formato hasta su impresión. *Lo que ves en la
pantalla, es lo que obtienes en el papel*.

Pero este enfoque tiene un problema recurrente en la edición digital: la lucha a 
favor de la uniformidad en los estilos. El descuido durante este método puede 
acarrear inconsistencias en la publicación que van en detrimento del cuidado 
editorial. Ejemplos de esto pueden ser:

*   Errores de estilo, como encabezados con la misma jerarquía pero con
    distinto tamaño de fuente, interlineado, tamaño de
    márgenes, etcétera.
*   Errores ortotipográficos, como párrafos con justificación forzada.
*   Errores en la creación de tablas de contenidos, referencias cruzadas
    o compaginación.

## WYSIWYM

Con el fin de solventar estos problemas surgió el enfoque
[WYSIWYM](https://es.wikipedia.org/wiki/WYSIWYM), acrónimo de *What You
See Is What You Mean*. Con esto no solo se hace patente una oposición al
enfoque WYSIWYG, sino un retorno a los lenguajes de marcado para la
estructuración semántica del contenido. El enfoque WYSIWYM y los lenguajes de 
marcado son similares ya que el marcaje, aunque afecta el aspecto de un texto, 
tiene la función de segmentar la información de una manera adecuada y 
significativa para las herramientas de *software*, el editor y el lector. Pero
la diferencia radica en que en el enfoque WYSIWYM se emplea un editor gráfico, 
semejante al procesador de texto, que disminuye drásticamente la curva de 
aprendizaje.

El equipo de desarrollo de [LyX](https://es.wikipedia.org/wiki/LyX) fue
el que propuso este enfoque cuando creó este programa de edición gráfica
con LaTeX, en 1999.

![Entorno de trabajo de LyX.](../img/s06_img02.jpg)

Para usuarios que no han tenido contacto con los lenguajes de marcado,
como lo es la mayoría de los escritores y editores, esta herramienta
puede resultar aún muy compleja. Por eso quizá una alternativa más
acorde a este contexto es el empleo de un [lenguaje de marcas
ligero](https://es.wikipedia.org/wiki/Lenguaje_de_marcas_ligero).

</section>
